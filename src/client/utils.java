package client;

import java.util.ArrayList;
import java.util.List;

public class utils {

    /**
     * Function used to calc Y values for repartition line chart
     * @param gradeList {@link List}<{@link Float}>
     * @return percentageList {@link List}<{@link Float}>
     */
    public static List<Float> getPercentage(List<Float> gradeList) {
        List<Float> percentageList = new ArrayList<>();
        float mean = 0;
        double std = (float) 0.1;
        float variance = 0;

        //calc mean
        for (float grade : gradeList) {
            mean += grade;
        }
        mean /= gradeList.size();

        //calc variance
        for (float grade : gradeList) {
            variance += Math.pow(grade - mean, 2);
        }
        variance /= gradeList.size();

        std = Math.sqrt(variance);

        for (float x : gradeList) {
            percentageList.add((float) Math.pow(Math.exp(-(((x - mean) * (x - mean)) / ((2 * variance)))), 1 / (std * Math.sqrt(2 * Math.PI))));
        }
        return percentageList;
    }
}
