package client;

public class DataExchange {
    String process;
    Object data;

    /**
     * Get function for process
     * @return process {@link String}
     */
    public String getProcess() {
        return process;
    }

    /**
     * Set function for process
     * @param process {@link String}
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * Get function for data
     * @return data {@link Object}
     */
    public Object getData() {
        return data;
    }

    /**
     * Set function for data
     * @param data {@link Object}
     */
    public void setData(Object data) {
        this.data = data;
    }
}