package client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Map;


public class ClientManager {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    /**
     * Contructor of Client Manager
     * It creates the socket et set up channels of communication
     */
    public ClientManager() {
        try {
            this.socket = new Socket("localhost", 8188);
            System.out.println("Connected");
            this.dis = new DataInputStream(socket.getInputStream());
            this.dos = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Function used to send message to the server
     * @param de {@link DataExchange}
     * @return the result of recieveMessage()
     */
    public DataExchange sendMessage(DataExchange de) {
        try {
            this.dos.writeUTF(new Gson().toJson(de));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return recieveMessage();
    }

    /**
     * Function used to read Message send by the server and put it in a DataExchange object
     * @return {@link DataExchange} object
     */
    public DataExchange recieveMessage() {
        String messageRecieved = "";
        try {
            messageRecieved = this.dis.readUTF();
            System.out.println(messageRecieved);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return new Gson().fromJson(messageRecieved, new TypeToken<DataExchange>() {
        }.getType());
    }

    /**
     * Function used to get the subjects studied by the level in param
     * @param level {@link String}
     * @return {@link List}<{@link String}> that contains the subject list
     */
    public List<String> requestLevelSubjects(String level) {
        DataExchange request = new DataExchange();
        request.setProcess("/subjects/" + level);
        DataExchange result = sendMessage(request);
        System.out.println(result.getData());
        return new Gson().fromJson(result.getData().toString(), new TypeToken<List<String>>() {
        }.getType());
    }

    /**
     * Function used to get the averages for a subject by the level
     * @param level {@link String}
     * @param subject {@link String}
     * @return {@link Map}<{@link String},{@link Float}> @key contains className, @value contains average for the subject
     */
    public Map<String, Float> requestClassSubjectAverage(String level, String subject) {
        DataExchange request = new DataExchange();
        request.setProcess("/average/" + level + "/" + subject);
        DataExchange result = sendMessage(request);
        System.out.println(result.getData());
        return new Gson().fromJson(result.getData().toString(), new TypeToken<Map<String, Float>>() {
        }.getType());
    }

    /**
     * Function used to get grades for a specific test
     * @param level {@link String}
     * @param subject {@link String}
     * @param test {@link String}
     * @return {@link List}<{@link Float}> grades container
     */
    public List<Float> requestTestGrades(String level, String subject, String test) {
        DataExchange request = new DataExchange();
        request.setProcess("/testGrades/" + level + "/" + subject + "/" + test);
        DataExchange result = sendMessage(request);
        System.out.println(result.getData());
        return new Gson().fromJson(result.getData().toString(), new TypeToken<List<Float>>() {
        }.getType());
    }

    /**
     * Function used to get general averages of a level
     * @param level {@link String}
     * @return {@link List}<{@link Float}> general averages container
     */
    public List<Float> requestGeneralAverages(String level) {
        DataExchange request = new DataExchange();
        request.setProcess("/general/" + level);
        DataExchange result = sendMessage(request);
        System.out.println(result.getData());
        return new Gson().fromJson(result.getData().toString(), new TypeToken<List<Float>>() {
        }.getType());
    }

    /**
     * Function used to get student average for a specific subject
     * @param level {@link String}
     * @param subject {@link String}
     * @return {@link List}<{@link Float}> subject average container
     */
    public List<Float> requestStudentsSubjectAverage(String level, String subject) {
        DataExchange request = new DataExchange();
        request.setProcess("/studentAverage/" + level + "/" + subject);
        DataExchange result = sendMessage(request);
        System.out.println(result.getData());
        return new Gson().fromJson(result.getData().toString(), new TypeToken<List<Float>>() {
        }.getType());
    }
}
