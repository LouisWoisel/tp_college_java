package sample;

import client.ClientManager;
import client.utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.*;

public class Controller implements Initializable {

    private ClientManager clientManager;

    /**********************Tab 1**********************/
    @FXML
    Button ConnectionButton;

    /**********************Tab 2**********************/
    @FXML
    BarChart<String, Float> SubjectAverageBarChart;

    @FXML
    ComboBox<String> SubjectAverageLevelChoice;

    @FXML
    ComboBox<String> SubjectAverageSubjectChoice;

    /**********************Tab 3**********************/

    @FXML
    ComboBox<String> GaussianTestLevelChoice;
    @FXML
    ComboBox<String> GaussianTestSubjectChoice;
    @FXML
    ComboBox<String> GaussianTestTestChoice;

    @FXML
    LineChart<String, Float> GaussianTestLineChart;

    /**********************Tab 4**********************/

    @FXML
    ComboBox<String> GeneralAverageLevelChoice;

    @FXML
    LineChart<String, Float> GeneralAverageLineChart;


    /**********************Tab 5**********************/
    @FXML
    ComboBox<String> StudentAverageLevelChoice;
    @FXML
    ComboBox<String> StudentAverageSubjectChoice;

    @FXML
    LineChart<String, Float> StudentAverageLineChart;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (int i = 3; i <= 6; i++) {
            SubjectAverageLevelChoice.getItems().add(String.format("%d", i));
            GaussianTestLevelChoice.getItems().add(String.format("%d", i));
            GeneralAverageLevelChoice.getItems().add(String.format("%d", i));
            StudentAverageLevelChoice.getItems().add(String.format("%d", i));
        }
        GaussianTestLineChart.getXAxis().setAnimated(false);
        GeneralAverageLineChart.getXAxis().setAnimated(false);
        StudentAverageLineChart.getXAxis().setAnimated(false);
    }

    public void connectButtonClick() {
        this.clientManager = new ClientManager();
    }

    public void displaySubjectAverage() {
        if (SubjectAverageLevelChoice.getValue() != null && SubjectAverageSubjectChoice.getValue() != null && this.clientManager != null) {
            String level = SubjectAverageLevelChoice.getValue();
            String subject = SubjectAverageSubjectChoice.getValue();
            Map<String, Float> averages = new HashMap<>();

            //request the list of averages
            averages = clientManager.requestClassSubjectAverage(level, subject);

            SubjectAverageBarChart.getData().clear();
            SubjectAverageBarChart.setTitle(subject + " averages for all class of level " + level);
            XYChart.Series<String, Float> dataSeries = new XYChart.Series<String, Float>();
            dataSeries.setName("Average for " + subject);

            //Add averages and className in dataSeries
            averages.forEach((classRoom, average) -> dataSeries.getData().add(new XYChart.Data<String, Float>(classRoom, average)));
            //Add data Series in BarChat
            SubjectAverageBarChart.getData().add(dataSeries);
        }
    }

    public void displayGaussianTest() {
        if (GaussianTestLevelChoice.getValue() != null && GaussianTestSubjectChoice.getValue() != null && GaussianTestTestChoice.getValue() != null && this.clientManager != null) {
            String level = GaussianTestLevelChoice.getValue();
            String subject = GaussianTestSubjectChoice.getValue();
            String test = GaussianTestTestChoice.getValue();
            List<Float> gradeList = new ArrayList<>();
            List<Float> percentageList = new ArrayList<>();

            gradeList = clientManager.requestTestGrades(level, subject, test);
            Collections.sort(gradeList);
            percentageList = utils.getPercentage(gradeList);

            GaussianTestLineChart.getData().clear();
            GaussianTestLineChart.setTitle("Gaussian for " + subject + " test " + test + " for level " + level);

            XYChart.Series<String, Float> dataSeries = new XYChart.Series<>();
            dataSeries.setName(subject);
            for (int i = 0; i < gradeList.size(); i++) {
                dataSeries.getData().add(new XYChart.Data<String, Float>(String.valueOf(gradeList.get(i)), percentageList.get(i)));
            }

            GaussianTestLineChart.getData().add(dataSeries);
        }
    }

    public void displayGeneralAverage() {
        if (GeneralAverageLevelChoice.getValue() != null && this.clientManager != null) {
            String level = GeneralAverageLevelChoice.getValue();
            List<Float> generalAverageList = new ArrayList<>();
            List<Float> percentageList = new ArrayList<>();

            generalAverageList = clientManager.requestGeneralAverages(level);
            Collections.sort(generalAverageList);
            percentageList = utils.getPercentage(generalAverageList);

            GeneralAverageLineChart.getData().clear();
            GeneralAverageLineChart.setTitle("Gaussian of general average for level " + level);

            XYChart.Series<String, Float> dataSeriesGeneral = new XYChart.Series<>();
            dataSeriesGeneral.setName(level);
            for (int i = 0; i < generalAverageList.size(); i++) {
                dataSeriesGeneral.getData().add(new XYChart.Data<String, Float>(String.valueOf(generalAverageList.get(i)), percentageList.get(i)));
            }

            GeneralAverageLineChart.getData().add(dataSeriesGeneral);
        }
    }

    public void displayStudentAverage() {
        if (StudentAverageLevelChoice.getValue() != null && StudentAverageSubjectChoice.getValue() != null && this.clientManager != null) {
            String level = StudentAverageLevelChoice.getValue();
            String subject = StudentAverageSubjectChoice.getValue();
            List<Float> studentAverageList = new ArrayList<>();
            List<Float> percentageList = new ArrayList<>();

            studentAverageList = clientManager.requestStudentsSubjectAverage(level, subject);
            Collections.sort(studentAverageList);
            percentageList = utils.getPercentage(studentAverageList);

            StudentAverageLineChart.getData().clear();
            StudentAverageLineChart.setTitle("Gaussian of general average for level " + level);

            XYChart.Series<String, Float> dataSeriesStudent = new XYChart.Series<>();
            dataSeriesStudent.setName(subject);
            for (int i = 0; i < studentAverageList.size(); i++) {
                dataSeriesStudent.getData().add(new XYChart.Data<String, Float>(String.valueOf(studentAverageList.get(i)), percentageList.get(i)));
            }

            StudentAverageLineChart.getData().add(dataSeriesStudent);
        }
    }

    public void fillSubjectChoice(ActionEvent ae) {
        if (ae.getSource().equals(SubjectAverageLevelChoice)) {

            for (String subject : clientManager.requestLevelSubjects(SubjectAverageLevelChoice.getValue())) {
                SubjectAverageSubjectChoice.getItems().add(subject);
            }
            displaySubjectAverage();
        } else if (ae.getSource().equals(GaussianTestLevelChoice)) {
            GaussianTestSubjectChoice.getItems().clear();

            for (String subject : clientManager.requestLevelSubjects(GaussianTestLevelChoice.getValue())) {
                GaussianTestSubjectChoice.getItems().add(subject);
            }
            displayGaussianTest();
        } else if (ae.getSource().equals(StudentAverageLevelChoice)) {
            StudentAverageSubjectChoice.getItems().clear();

            for (String subject : clientManager.requestLevelSubjects(StudentAverageLevelChoice.getValue())) {
                StudentAverageSubjectChoice.getItems().add(subject);
            }
            displayStudentAverage();
        }
    }

    public void fillTestChoice() {
        GaussianTestTestChoice.getItems().clear();
        int j;
        if (GaussianTestSubjectChoice.getValue().equals("Musique") || GaussianTestSubjectChoice.getValue().equals("Sport")) {
            j = 2;
        } else {
            j = 3;
        }
        for (int i = 1; i <= j; i++) {
            GaussianTestTestChoice.getItems().add(String.format("%d", i));
        }
        displayGaussianTest();
    }

    public void clearCharts() {
        SubjectAverageSubjectChoice.getItems().clear();
        GaussianTestLineChart.getData().clear();
        GeneralAverageLineChart.getData().clear();
        StudentAverageLineChart.getData().clear();
    }
}