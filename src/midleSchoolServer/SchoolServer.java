package midleSchoolServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SchoolServer {
    public static void main(String[] args) {
        midleSchoolManager.createSchool();
        try (ServerSocket serverSocket = new ServerSocket(8188)) {
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");

                new ServerThread(socket).start();
            }
        } catch (IOException e) {
            System.out.println("Server exception : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
