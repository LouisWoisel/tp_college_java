package midleSchoolServer.midleSchoolStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to create an establishment
 */
public class Establishment {
    private List<ClassRoom> classRooms;

    /**
     * Constructor of Establishment
     */
    public Establishment() {
        classRooms = new ArrayList<>();
        String className = "";
        for (int i = 6; i >= 3; i--) {
            for (int j = 0; j < 6; j++) {
                className += i;
                className += (char) ('A' + j);
                classRooms.add(new ClassRoom(className, i));
                className = "";
            }
        }
    }

    /**
     * Function used to get a class room by its name
     * @param classRoomName {@link String}
     * @return classRoom {@link ClassRoom}
     */
    public ClassRoom getClassRoomByName(String classRoomName) {
        for (ClassRoom classRoom : this.getClassRooms()) {
            if (classRoom.getName().equals(classRoomName)) {
                return classRoom;
            }
        }
        return null;
    }

    /**
     * Function used to get a student by its name
     * @param studentName {@link String}
     * @return student {@link Student}
     */
    public Student getStudentByName(String studentName) {
        String className = studentName.substring(0, 2);
        ClassRoom studentClass = getClassRoomByName(className);
        for (Student student : studentClass.getStudents()) {
            if (student.getName().equals(studentName)) {
                return student;
            }
        }
        return null;
    }

    /**
     * Function used to get all class room of a level
     * @param level {@link Integer}
     * @return classRoomList {@link List}<{@link ClassRoom}>
     */
    public List<ClassRoom> getAllClassRoomFromLevel(int level) {
        List<ClassRoom> classRoomList = new ArrayList<>();
        for (ClassRoom classRoom : this.getClassRooms()) {
            if (classRoom.getLevel() == level) {
                classRoomList.add(classRoom);
            }
        }
        classRoomList.sort((a, b) -> a.getName().compareTo(b.getName()));
        return classRoomList;
    }

    /**
     * Get function for Establishment classRooms
     *
     * @return classRooms {@link List}<{@link ClassRoom}>
     */
    public List<ClassRoom> getClassRooms() {
        return classRooms;
    }

    /**
     * Set function for Establishment classRooms
     *
     * @param classRooms {@link List}<{@link ClassRoom}>
     */
    public void setClassRooms(List<ClassRoom> classRooms) {
        this.classRooms = classRooms;
    }
}
