package midleSchoolServer.midleSchoolStructure;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class used to create a Student
 */
public class Student {

    private String name;
    private List<Subject> subjects;

    private static final String[] mandatorySubject = {
            "Mathématique",
            "Français",
            "Anglais",
            "Histoire-Géographie",
            "Physique",
            "Sciences-Naturelles",
            "Arts",
            "Musique",
            "Sport",
            "Langue-Vivantes"
    };

    private static final String[] optionalSubject = {
            "Latin",
            "Grec",
            "Anglais-Avancé"
    };

    /**
     * Constructor of Student
     *
     * @param name  {@link String}   Name of the student
     * @param level {@link Integer}     Level of the student
     */
    public Student(String name, int level) {
        this.name = name;
        subjects = new ArrayList<>();
        for (String subjectName : mandatorySubject) {
            if (level == 6 && subjectName.equals("Physique")) {
            } else if (level == 6 && subjectName.equals("Langue-Vivantes")) {
            } else {
                subjects.add(new Subject(subjectName));
            }
        }
        Random rand = new Random();
        int nbOptional = rand.nextInt(3);
        int rand2 = 0;
        switch (nbOptional) {
            case 1:
                //nbOptional = 1 then take a random optional
                rand2 = rand.nextInt(3);
                subjects.add(new Subject(optionalSubject[rand2]));
                break;
            case 2:
                //nbOptional = 2 then take "Anglais-Avancé" + random between "Latin" and "Grec"
                subjects.add(new Subject(optionalSubject[2]));
                rand2 = rand.nextInt(2);
                subjects.add(new Subject(optionalSubject[rand2]));
                break;
            default:
                //0 optional
                break;
        }
    }

    /**
     * Function used to get student average for a subject
     * @param subjectName {@link String}
     * @return average {@link Float}
     */
    public float getAverageForSubject(String subjectName) {
        float average = 0;
        int count = 0;
        for (Subject subjects : this.getSubjects()) {
            if (subjects.getName().equals(subjectName)) {
                for (float grade : subjects.getGrades()) {
                    average += grade;
                    count++;
                }
            }
        }
        average /= count;
        BigDecimal bigDecimal = BigDecimal.valueOf(average);
        return bigDecimal.setScale(2, RoundingMode.HALF_DOWN).floatValue();
    }

    /**
     * Function used to get student general average
     * @return generalAverage {@link Float}
     */
    public float getGeneralAverageOfStudent() {
        float generalAverage = 0;
        float bonusPoint = 0;
        int count = 0;
        for (Subject subject : this.getSubjects()) {
            for (float grade : subject.getGrades()) {
                if (subject.getName().equals("Latin") || subject.getName().equals("Grec") || subject.getName().equals("Anglais-Avancé")) {
                    if (grade >= 10) {
                        bonusPoint += 0.1;
                    }
                } else {
                    generalAverage += grade;
                    count++;
                }
            }
        }
        generalAverage /= count;
        BigDecimal bd = BigDecimal.valueOf(generalAverage);
        return bd.setScale(2, RoundingMode.DOWN).floatValue() + bonusPoint;
    }

    /**
     * Get function for Student.name
     *
     * @return name {@link String} Name of the student
     */
    public String getName() {
        return name;
    }

    /**
     * Set function for Student.name
     *
     * @param name {@link String} Name of the student
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get function for the Student list of subjects
     *
     * @return subjects {@link List}<{@link Subject}> List of subjects
     */
    public List<Subject> getSubjects() {
        return subjects;
    }

    /**
     * Set function for the Student list of subject
     *
     * @param subjects {@link List}<{@link Subject}>
     */
    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
