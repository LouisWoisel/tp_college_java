package midleSchoolServer.midleSchoolStructure;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Class used to create a Subject
 */
public class Subject {
    private String name = "";
    private List<Float> grades;

    /**
     * Constructor of Subject
     *
     * @param name {@link String}
     */
    public Subject(String name) {
        this.name = name;
        grades = new ArrayList<>();
        randomGrades();
    }

    /**
     * Function that generate random grades
     */
    public void randomGrades() {
        Random random = new Random();
        BigDecimal bd;
        if (this.name.equals("Sport") || this.name.equals("Musique")) {
            for (int i = 0; i < 2; i++) {
                bd = BigDecimal.valueOf((Math.abs(random.nextGaussian() * 4) + 10) % 20);
                grades.add(bd.setScale(2, RoundingMode.HALF_DOWN).floatValue());
            }
        } else {
            for (int i = 0; i < 3; i++) {
                bd = BigDecimal.valueOf((Math.abs(random.nextGaussian() * 4) + 10) % 20);
                grades.add(bd.setScale(2, RoundingMode.HALF_DOWN).floatValue());
            }
        }
    }

    /**
     * Get Function for Subject name
     *
     * @return name {@link String}
     */
    public String getName() {
        return name;
    }

    /**
     * Set Function for Subject name
     *
     * @param name {@link String}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get function for Subject grades
     *
     * @return grades {@link List}<{@link Float}>
     */
    public List<Float> getGrades() {
        return grades;
    }

    /**
     * Set function for Subject grades
     *
     * @param grades {@link List}<{@link Float}>
     */
    public void setGrades(List<Float> grades) {
        this.grades = grades;
    }
}
