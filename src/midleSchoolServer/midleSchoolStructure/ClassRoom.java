package midleSchoolServer.midleSchoolStructure;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import java.util.Comparator;
import java.util.List;

/**
 * Class used to create a class room
 */
public class ClassRoom {
    private String name;
    private int level;
    private List<Student> students;

    /**
     * Constructor of ClassRoom
     *
     * @param name  {@link String}
     * @param level {@link Integer}
     */
    public ClassRoom(String name, int level) {
        this.name = name;
        this.level = level;
        students = new ArrayList<>();
        String studentName;
        for (int i = 0; i < 20; i++) {
            studentName = name + "-Student";
            studentName += (char) ('A' + i);
            students.add(new Student(studentName, level));
        }
    }

    /**
     * Function that return all grades of the classRoom for the subject given in param
     *
     * @param subjectName {@link String}
     * @return gradesList {@link List}<{@link Float}>
     */
    private List<Float> getGradesFromSubject(String subjectName) {
        List<Float> gradeList = new ArrayList<>();
        for (Student students : this.getStudents()) {
            for (Subject subject : students.getSubjects()) {
                if (subject.getName().equals(subjectName)) {
                    gradeList.addAll(subject.getGrades());
                }
            }
        }
        return gradeList;
    }

    /**
     * Funtion used to list class room grades for a specific test
     * @param subjectName {@link String}
     * @param test {@link Integer}
     * @return gradeList {@link List}<{@link Float}>
     */
    public List<Float> getGradesFromTest(String subjectName, int test) {
        List<Float> gradeList = new ArrayList<>();
        for (Student students : this.getStudents()) {
            for (Subject subject : students.getSubjects()) {
                if (subject.getName().equals(subjectName)) {
                    gradeList.add(subject.getGrades().get(test - 1));
                }
            }
        }
        return gradeList;
    }

    /**
     * Function used to get the class room max grade of a subject
     * @param subjectName {@link String}
     * @return max {@link Float}
     */
    public float getMaxFromSubject(String subjectName) {
        float max = 0;
        List<Float> gradeList = getGradesFromSubject(subjectName);
        for (Float grade : gradeList) {
            if (grade > max) {
                max = grade;
            }
        }
        return max;
    }

    /**
     * Function used to get the class room min for a subject
     * @param subjectName {@link String}
     * @return min {@link Float}
     */
    public float getMinFromSubject(String subjectName) {
        float min = 20;
        List<Float> gradeList = getGradesFromSubject(subjectName);
        for (Float grade : gradeList) {
            if (grade < min) {
                min = grade;
            }
        }
        return min;
    }

    /**
     * Function used to get the class room median for a subject
     * @param subjectName {@link String}
     * @return median {@link Float}
     */
    public float getMedianFromSubject(String subjectName) {
        float median = 0;
        List<Float> gradeList = getGradesFromSubject(subjectName);
        gradeList.sort(Comparator.comparingDouble(Float::floatValue));
        if (gradeList.size() % 2 == 0) {
            median = gradeList.get((gradeList.size() / 2));
        } else {
            median = gradeList.get((int) (gradeList.size() / 2) + 1);
        }
        return median;
    }

    /**
     * Function used to getgeneral average of all student in class room
     * @return generalAverage {@link List}<{@link Float}>
     */
    public List<Float> getClassRoomStudentsGeneralAverage() {
        List<Float> generalAverage = new ArrayList<>();
        for (Student student : this.getStudents()) {
            generalAverage.add(student.getGeneralAverageOfStudent());
        }
        return generalAverage;
    }

    /**
     * Funtion used to get average of all student in class room for a subject
     * @param subjectName {@link String}
     * @return classRoomStudentsSubjectAverage {@link List}<{@link Float}>
     */
    public List<Float> getStudentsAverageForSubject(String subjectName) {
        List<Float> classRoomStudentsSubjectAverage = new ArrayList<>();
        for (Student students : this.getStudents()) {
            for (Subject subject : students.getSubjects())
                if (subject.getName().equals(subjectName)) {
                    classRoomStudentsSubjectAverage.add(students.getAverageForSubject(subjectName));
                }

        }
        return classRoomStudentsSubjectAverage;
    }

    /**
     * Function that calculate the average of a classRoom for one subject
     *
     * @param subjectName {@link String}
     * @return average {@link Float}
     */
    public float getAverageFromSubject(String subjectName) {
        float average = 0;
        int count = 0;
        List<Float> gradeList = getGradesFromSubject(subjectName);
        for (Float grade : gradeList) {
            average += grade;
            count++;
        }
        average /= count;
        BigDecimal bd = BigDecimal.valueOf(average);
        return bd.setScale(2, RoundingMode.DOWN).floatValue();
    }

    /**
     * Function used to get all subject studied by the class room
     * @return classSubject {@link List}<{@link String}>
     */
    public List<String> getSubjectOfClass() {
        List<String> classSubject = new ArrayList<>();
        for (Student student : this.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                if (!classSubject.contains(subject.getName())) {
                    classSubject.add(subject.getName());
                }
            }
        }
        return classSubject;
    }

    /**
     * Get function for ClassRoom name
     *
     * @return name {@link String}
     */
    public String getName() {
        return name;
    }

    /**
     * Set function for ClassRoom name
     *
     * @param name {@link String}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get function for ClassRoom students
     *
     * @return students {@link List}<{@link Student}>
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * Set function for ClassRoom students
     *
     * @param students {@link List}<{@link Student}>
     */
    public void setStudents(List<Student> students) {
        this.students = students;
    }

    /**
     * Get function for ClassRoom level
     *
     * @return level {@link Integer}
     */
    public int getLevel() {
        return level;
    }

    /**
     * Set function for ClassRoom level
     *
     * @param level {@link Integer}
     */
    public void setLevel(int level) {
        this.level = level;
    }
}
