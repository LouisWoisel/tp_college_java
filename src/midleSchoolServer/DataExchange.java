package midleSchoolServer;

public class DataExchange {
    String process;
    Object data;

    /**
     * Function used to set the response to the request : testGrades
     */
    public void computeTestGrades() {
        String level = this.process.split("/")[2];
        String subject = this.process.split("/")[3];
        String test = this.process.split("/")[4];

        this.data = midleSchoolManager.getGradeListForTestFromSubject(Integer.parseInt(level), subject, Integer.parseInt(test));
    }

    /**
     * Function used to set the response to the request : average
     */
    public void computeAverage() {
        String level = this.process.split("/")[2];
        String subject = this.process.split("/")[3];

        this.data = midleSchoolManager.getClassRoomAveragesForSubjectFromLevel(Integer.parseInt(level), subject);
    }

    /**
     * Function used to set the response to the request : general
     */
    public void computeGeneral() {
        String level = this.process.split("/")[2];

        this.data = midleSchoolManager.getGeneralAverageOfAllStudentFromLevel(Integer.parseInt(level));
    }

    /**
     * Function used to set the response to the request : subject
     */
    public void computeSubject() {
        String level = this.process.split("/")[2];
        this.data = midleSchoolManager.getSubjectForLevel(Integer.parseInt(level));
    }

    /**
     * Function used to set the response to the request : studentAverage
     */
    public void computeStudentAverage() {
        String level = this.process.split("/")[2];
        String subject = this.process.split("/")[3];
        this.data = midleSchoolManager.getLevelAveragesForSubject(Integer.parseInt(level), subject);
    }

    /**
     * Get function for process
     * @return process {@link String}
     */
    public String getProcess() {
        return process;
    }

    /**
     * Set function for process
     * @param process {@link String}
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * Get function for data
     * @return data {@link Object}
     */
    public Object getData() {
        return data;
    }

    /**
     * Set function for data
     * @param data {@link Object}
     */
    public void setData(Object data) {
        this.data = data;
    }
}
