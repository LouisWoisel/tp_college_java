package midleSchoolServer;

import com.google.gson.Gson;
import midleSchoolServer.midleSchoolStructure.ClassRoom;
import midleSchoolServer.midleSchoolStructure.Establishment;
import midleSchoolServer.midleSchoolStructure.Student;
import midleSchoolServer.midleSchoolStructure.Subject;

import java.io.*;
import java.util.*;

public class midleSchoolManager {
    /**
     * Function that create a school in write it in json
     */
    public static void createSchool() {
        Establishment establishment = new Establishment();
        Gson gson = new Gson();
        try {
            File jsonFile = new File("school.json");
            if (!jsonFile.exists()) {
                jsonFile.createNewFile();
                System.out.println("File created.");
            }
            FileWriter writer = new FileWriter("school.json");
            writer.write(gson.toJson(establishment));
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Function that read the file school.json
     *
     * @return establishment {@link Establishment}
     */
    public static Establishment readSchool() {
        Gson gson = new Gson();
        Establishment establishmentRead = null;
        try (FileReader reader = new FileReader("school.json")) {
            establishmentRead = gson.fromJson(reader, Establishment.class);
            //System.out.println(gson.toJson(establishmentRead));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return establishmentRead;
    }

    /**
     * Function that print all grades from all student in the class given in param
     *
     * @param className {@link String}
     */
    public static void getAllGradesFromClass(String className) {
        Establishment establishment = readSchool();
        List<Float> gradeList = new ArrayList<>();
        ClassRoom classRoom = establishment.getClassRoomByName(className);
        for (Student student : classRoom.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                gradeList.addAll(subject.getGrades());
            }
        }
        System.out.println(gradeList);
    }

    /**
     * Function that print all grades from student in param
     *
     * @param studentName {@link String}
     */
    public static void getAllGradesFromStudent(String studentName) {
        Establishment establishment = readSchool();
        List<Float> gradeList = new ArrayList<>();
        Student student = establishment.getStudentByName(studentName);
        for (Subject subject : student.getSubjects()) {
            gradeList.addAll(subject.getGrades());
        }
        System.out.println(gradeList);

    }


    /**
     * Function that print all grades from all student in the level given in param
     *
     * @param level {@link Integer}
     */
    public static void getAllGradesFromLevel(int level) {
        Establishment establishment = readSchool();
        List<ClassRoom> classRoomList = establishment.getAllClassRoomFromLevel(level);
        List<Float> gradeList = new ArrayList<>();
        for (ClassRoom classroom : classRoomList) {
            for (Student student : classroom.getStudents()) {
                for (Subject subject : student.getSubjects()) {
                    gradeList.addAll(subject.getGrades());
                }
            }
        }
        System.out.println(gradeList);
    }

    /**
     * Function that Calculate all the averages of all Subjects of the classRoom given in param
     *
     * @param className {@link String}
     */
    public static Map<String, Float> getAveragesFromClass(String className) {
        Establishment establishment = readSchool();
        Map<String, Float> averages = new HashMap<>();
        ClassRoom classRoom = establishment.getClassRoomByName(className);
        for (Student student : classRoom.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                if (!averages.containsKey(subject.getName())) {
                    averages.put(subject.getName(), classRoom.getAverageFromSubject(subject.getName()));
                }
            }
        }
        return averages;
    }

    public static void getMaxFromClass(String className) {
        Establishment establishment = readSchool();
        Map<String, Float> maxs = new HashMap<>();
        ClassRoom classRoom = establishment.getClassRoomByName(className);
        for (Student student : classRoom.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                if (!maxs.containsKey(subject.getName())) {
                    maxs.put(subject.getName(), classRoom.getMaxFromSubject(subject.getName()));
                }
            }
        }
        System.out.println(maxs);
    }

    public static void getMinFromClass(String className) {
        Establishment establishment = readSchool();
        Map<String, Float> mins = new HashMap<>();
        ClassRoom classRoom = establishment.getClassRoomByName(className);
        for (Student student : classRoom.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                if (!mins.containsKey(subject.getName())) {
                    mins.put(subject.getName(), classRoom.getMinFromSubject(subject.getName()));
                }
            }
        }
        System.out.println(mins);
    }

    public static void getMedianFromClass(String className) {
        Establishment establishment = readSchool();
        Map<String, Float> medians = new HashMap<>();
        ClassRoom classRoom = establishment.getClassRoomByName(className);
        for (Student student : classRoom.getStudents()) {
            for (Subject subject : student.getSubjects()) {
                if (!medians.containsKey(subject.getName())) {
                    medians.put(subject.getName(), classRoom.getMedianFromSubject(subject.getName()));
                }
            }
        }
        System.out.println(medians);
    }

    public static Map<String, Float> getClassRoomAveragesForSubjectFromLevel(int level, String subjectName) {
        Map<String, Float> subjectAverages = new TreeMap<>();
        Establishment establishment = readSchool();
        for (ClassRoom classRoom : establishment.getAllClassRoomFromLevel(level)) {
            subjectAverages.put(classRoom.getName(), classRoom.getAverageFromSubject(subjectName));
        }
        return subjectAverages;
    }

    public static List<Float> getGradeListForTestFromSubject(int level, String subjectName, int test) {
        List<Float> gradeList = new ArrayList<>();
        Establishment establishment = readSchool();
        for (ClassRoom classRoom : establishment.getAllClassRoomFromLevel(level)) {
            gradeList.addAll(classRoom.getGradesFromTest(subjectName, test));
        }
        return gradeList;
    }

    public static List<Float> getGeneralAverageOfAllStudentFromLevel(int level) {
        Establishment establishment = readSchool();
        List<Float> generalAverages = new ArrayList<>();
        for (ClassRoom classRoom : establishment.getAllClassRoomFromLevel(level)) {
            generalAverages.addAll(classRoom.getClassRoomStudentsGeneralAverage());
        }
        return generalAverages;
    }

    public static List<String> getSubjectForLevel(int level) {
        Establishment establishment = readSchool();
        List<String> levelSubjects = new ArrayList<>();
        for (ClassRoom classRoom : establishment.getAllClassRoomFromLevel(level)) {
            for (String subjectName : classRoom.getSubjectOfClass()) {
                if (!levelSubjects.contains(subjectName)) {
                    levelSubjects.add(subjectName);
                }
            }
        }
        return levelSubjects;
    }

    public static List<Float> getLevelAveragesForSubject(int level, String subjectName) {
        Establishment establishment = readSchool();
        List<Float> subjectAverage = new ArrayList<>();
        for (ClassRoom classRoom : establishment.getAllClassRoomFromLevel(level)) {
            subjectAverage.addAll(classRoom.getStudentsAverageForSubject(subjectName));
        }
        return subjectAverage;
    }
}
