package midleSchoolServer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    public ServerThread(Socket socket) {
        this.socket = socket;
        try {
            this.dis = new DataInputStream(socket.getInputStream());
            this.dos = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void run() {
        while (true) {
            try {
                String messageRecieved = this.dis.readUTF();
                System.out.println(messageRecieved);
                Gson gson = new Gson();
                DataExchange de = gson.fromJson(messageRecieved, new TypeToken<DataExchange>() {
                }.getType());
                String[] deString = de.getProcess().split("/");
                switch (deString[1]) {
                    case "testGrades":
                        de.computeTestGrades();
                        sendMessage(de);
                        break;
                    case "average":
                        de.computeAverage();
                        sendMessage(de);
                        break;
                    case "general":
                        de.computeGeneral();
                        sendMessage(de);
                        break;
                    case "subjects":
                        de.computeSubject();
                        sendMessage(de);
                        break;
                    case "studentAverage":
                        de.computeStudentAverage();
                        sendMessage(de);
                    default:
                        break;
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.out.println("Client disconnected");
                return;
            }
        }
    }

    public void sendMessage(DataExchange de) {
        try {
            this.dos.writeUTF(new Gson().toJson(de));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
