# TP10_LouisWoisel

Ce document est utilisé pour expliquer l'architecture et le fonctionnement du programme.

## Fonctionnement de l'application

Ce projet utilise une partie client et une partie serveur, les deux communiquent avec des Server Socket.

### 1 - Serveur
Le main du serveur est dans la classe /midleSchoolServer/SchoolServer.java, c'est ce main qu'il faut lancé pour activer la partie "backend" de l'application.
Le serveur crée un thread par connexion pour que l'on puisse connecter plusieurs client pour eviter les conflits.

Tous les envois et reçus de données se font par le biais d'un json dont la structure se base sur la classe /midleSchoolServer/DataExchange.java

Les traitements et calculs se font avec la classe /midleSchoolServer/midleSchoolManager.java cette classe utilise l'architecture école crée dans le package /midleSchoolServer/midleSchoolServer.midleSchoolStrucure

Au lancement du serveur un fichier school.json est crée avec comme contenu toute la génération d'une école.

### 2 - Client
La partie client est un application crée avec la librairie JFX.

Cette application permet l'affichage de différents histogrammes et courbes utilisants des paramètres séléctionable par l'utilisateur.

Dans le dossier sample on peut trouver les éléments relatifs à la librairie JFX.

Et dans le dossier client se trouve les classes relatives aux différents processus tel que l'envoi/réception de donnée.

Le main de l'application client se trouve dans la classe sample/Main.java

### 3 - Parties réalisées
Les sections 1 à 4 du sujet ont été réalisées,
la partie 5 n'est pas implémentée dans mon projet.
